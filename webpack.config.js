const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
  template: './src/index.html',
  filename: './index.html',
  inject: 'body'
});

const CopyWebpackPluginConfig = new CopyWebpackPlugin([
  { from: './src/img', to: 'img' },
  { from: './src/fonts', to: 'fonts' }
]);

const CleanWebpackPluginConfig = new CleanWebpackPlugin('build', {});

module.exports = {
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'index.js'
  },
  resolve: {
    alias: {
      components: path.resolve(__dirname, './src/components'),
      data: path.resolve(__dirname, './src/data'),
      constants: path.resolve(__dirname, './src/helpers/constants.js'),
      mixins: path.resolve(__dirname, './src/helpers/mixins.js'),
      utils: path.resolve(__dirname, './src/helpers/utils.js'),
      img: path.resolve(__dirname, './src/img')
    }
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      },
      {
        test: /\.(otf)$/,
        use: [
          {
            loader: 'file-loader',
            options: {}
          }
        ]
      },
      {
        test: /\.(png|jpg|svg)$/,
        use: [
          {
            loader: 'file-loader',
            options: {}
          }
        ]
      }
    ]
  },
  plugins: [HtmlWebpackPluginConfig, CopyWebpackPluginConfig, CleanWebpackPluginConfig]
};

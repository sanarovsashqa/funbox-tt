export const screenWidth = {
  MOBILE: 320,
  TABLET: 768,
  DESKTOP: 1280
};

export const wordEnding = {
  PORTIONS: ['порция', 'порции', 'порций'],
  MICE: ['мышь', 'мыши', 'мышей']
};

export const color = {
  card: {
    DEFAULT: '#1698d9',
    SELECTED: '#d91667',
    DISABLED: '#b3b3b3'
  },
  cardHover: {
    DEFAULT: '#2ea8e6',
    SELECTED: '#e62e7a'
  },
  cardText: {
    DEFAULT: '#666666',
    SELECTED: '#e62e7a'
  },
  description: {
    DEFAULT: '#ffffff',
    DISABLED: '#ffff66'
  },
};

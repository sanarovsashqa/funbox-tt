import { css } from 'styled-components';
import { screenWidth } from 'constants';

const media = (width) => Object.keys(width).reduce((acc, label) => {
  acc[label] = (...args) => css`
    @media (min-width: ${width[label] / 16}em) {
      ${css(...args)}
    }
  `;
  return acc;
}, {});

export const MEDIA = media(screenWidth);

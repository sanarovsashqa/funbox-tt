import React from 'react';
import ReactDOM from 'react-dom';
import App from 'components/App';
import styledNormalize from 'styled-normalize';
import { injectGlobal } from 'styled-components';
import { MEDIA } from 'mixins';
import { screenWidth } from 'constants';
import cssVars from 'css-vars-ponyfill';

injectGlobal`
  ${styledNormalize}

  @font-face {
    font-family: 'Exo 2';
    font-style: normal;
    font-weight: 100;
    src: url('./fonts/Exo2.0-Thin.otf') format('opentype');
  }

  html {
    box-sizing: border-box;
  }

  *,
  *::before,
  *::after {
    box-sizing: inherit;
  }

  #root {
    position: relative;
    min-width: 320px;
    font-family: var(--font-trebuchet);
    font-size: 14px;
    line-height: 1.2;
    font-weight: 400;
    color: var(--white-color);
    background-color: var(--black-color);
    background: url('img/pattern.jpg');

    &::before {
      content: '';
      position: absolute;
      display: block;
      width: 100%;
      height: 100%;
      background: linear-gradient(0deg, rgba(0, 0, 0, 1) 0%, rgba(0, 0, 0, 0) 50%, rgba(0 ,0 ,0, 1) 100%);
      opacity: 0.5;
    }
  }

  .container {
    width: 320px;
    margin-left: auto;
    margin-right: auto;

    ${MEDIA.TABLET`
      width: ${screenWidth.TABLET}px;
      padding-right: 40px;
      padding-left: 40px;
    `}

    ${MEDIA.DESKTOP`
      width: ${screenWidth.DESKTOP}px;
      padding-right: 80px;
      padding-left: 80px;
    `}
  }

  :root {
    --black-color: #000000;
    --white-color: #ffffff;
    --light-gray-color: #f2f2f2;
    --dark-gray-color: #666666;
    --light-blue-color: #1698d9;
    --pink-color: #d91667;

    --font-exo2: 'Exo 2', Helvetica, sans-serif;
    --font-trebuchet: 'Trebuchet MS', Helvetica, sans-serif;
  }
`;

cssVars();

ReactDOM.render(<App />, document.querySelector('#root'));

import React from 'react';
import styled from 'styled-components';
import Cards from 'components/Cards';
import Title from './Title';

const StyledMain = styled.main`
  position: relative;
  z-index: 1;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  min-height: 100vh;
  padding-top: 20px;
`;

const Main = ({ data }) => {
  return (
    <StyledMain className="container">
      <Title>Ты сегодня покормил кота?</Title>
      <Cards data={data} />
    </StyledMain>
  );
};

export default Main;

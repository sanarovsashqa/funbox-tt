import styled from 'styled-components';

const Title = styled.h1`
  margin: 0;
  font-family: var(--font-exo2);
  font-size: 36px;
  font-weight: 100;
  text-align: center;
`;

export default Title;

import React from 'react';
import styled from 'styled-components';
import Card from 'components/Card';
import { MEDIA } from 'mixins';

const StyledCards = styled.ul`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  margin: 0;
  padding: 25px 0;
  list-style: none;

  ${MEDIA.TABLET`
    margin-left: -40px;
  `}

  ${MEDIA.DESKTOP`
    margin-left: -80px;
    justify-content: flex-start;
  `};
`;

const Cards = ({ data }) => {
  const cards = data.map((cardData, index) => {
    return <Card key={index} data={cardData} />;
  });

  return <StyledCards>{cards}</StyledCards>;
};

export default Cards;

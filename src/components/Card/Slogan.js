import React from 'react';
import styled from 'styled-components';
import { color } from 'constants';

const StyledSlogan = styled.p`
  margin: 0;
  margin-bottom: 5px;
  font-size: 16px;
  color: ${props => props.color};
`;

const Slogan = props => {
  if (props.isSelected) {
    if (props.isMouseOver && !props.isFirstHover) {
      return <StyledSlogan color={color.cardText.SELECTED}>Котэ не одобряет?</StyledSlogan>;
    }
  }

  return <StyledSlogan color={color.cardText.DEFAULT}>{props.slogan}</StyledSlogan>;
};

export default Slogan;

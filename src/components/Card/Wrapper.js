import styled from 'styled-components';
import { MEDIA } from 'mixins';

const Wrapper = styled.li`
  position: relative;
  margin-bottom: 20px;

  ${MEDIA.TABLET`
    margin-left: 40px;
  `}

  ${MEDIA.DESKTOP`
    margin-left: 80px;
  `};
`;

export default Wrapper;

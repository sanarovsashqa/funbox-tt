import React from 'react';
import styled from 'styled-components';
import { getEnding } from 'utils';
import { wordEnding } from 'constants';

const StyledGifts = styled.p`
  margin: 0;
  font-size: 14px;
  line-height: 16px;
  color: var(--dark-gray-color);
`;

const GiftsNum = styled.span`
  font-weight: 900;
`;

const Gifts = props => {
  return (
    <StyledGifts>
      <GiftsNum>{props.giftsNum !== 1 ? props.giftsNum : null}</GiftsNum>{' '}
      {getEnding(props.giftsNum, wordEnding.MICE)} в подарок
    </StyledGifts>
  );
};

export default Gifts;

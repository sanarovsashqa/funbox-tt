import styled from 'styled-components';

const Name = styled.h2`
  margin: 0;
  font-size: 48px;
  color: var(--black-color);
  overflow: hidden;
  text-overflow: ellipsis;
`;

export default Name;

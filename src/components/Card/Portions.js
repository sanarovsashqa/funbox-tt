import React from 'react';
import styled from 'styled-components';
import { getEnding } from 'utils';
import { wordEnding } from 'constants';

const StyledPortions = styled.p`
  margin: 0;
  font-size: 14px;
  line-height: 16px;
  color: var(--dark-gray-color);
`;

const PortionsNum = styled.span`
  font-weight: 900;
`;

const Portions = props => {
  return (
    <StyledPortions>
      <PortionsNum>{props.portionsNum}</PortionsNum>{' '}
      {getEnding(props.portionsNum, wordEnding.PORTIONS)}
    </StyledPortions>
  );
};

export default Portions;

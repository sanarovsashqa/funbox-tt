import React from 'react';
import styled from 'styled-components';
import { color } from 'constants';

const StyledDescription = styled.p`
  max-width: 320px;
  margin: 0;
  font-size: 13px;
  line-height: 16px;
  color: ${props => props.color};
  text-align: center;
`;

const DescriptionLink = styled.a`
  font-weight: 900;
  color: var(--light-blue-color);
  text-decoration: none;
  border-bottom: 1px dashed var(--light-blue-color);
  cursor: pointer;
`;

const Description = props => {
  if (props.isDisabled) {
    return (
      <StyledDescription color={color.description.DISABLED}>
        Печалька, {props.tasteInfo} закончился.
      </StyledDescription>
    );
  } else if (props.isSelected) {
    return (
      <StyledDescription color={color.description.DEFAULT}>
        {props.description}
      </StyledDescription>
    );
  }

  return (
    <StyledDescription color={color.description.DEFAULT}>
      Чего сидишь? Порадуй котэ,{' '}
      <DescriptionLink onClick={props.onClick}>купи.</DescriptionLink>
    </StyledDescription>
  );
};

export default Description;

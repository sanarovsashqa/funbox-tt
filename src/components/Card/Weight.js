import React from 'react';
import styled from 'styled-components';

const WeightWrapper = styled.div`
  position: absolute;
  right: 15px;
  bottom: 15px;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  align-items: center;
  width: 80px;
  height: 80px;
  padding-top: 5px;
  border-radius: 50%;
  background-color: ${props => props.color};
  overflow: hidden;
`;

const StyledWeight = styled.p`
  margin: 0;
  font-size: 21px;
  color: var(--white-color);
  text-align: center;
`;

const WeightNum = styled.span`
  display: block;
  font-size: 42px;
  line-height: 42px;
`;

const Weight = props => {

  return (
    <WeightWrapper color={props.color}>
      <StyledWeight>
        <WeightNum>{props.weight}</WeightNum> кг
      </StyledWeight>
    </WeightWrapper>
  );
};

export default Weight;

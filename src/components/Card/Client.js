import styled from 'styled-components';

const Client = styled.p`
  margin: 0;
  font-size: 14px;
  line-height: 16px;
  color: var(--dark-gray-color);
`;

export default Client;

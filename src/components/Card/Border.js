import React from 'react';
import styled from 'styled-components';

const StyledBorder = styled.div`
  position: relative;
  margin-bottom: 10px;
  padding: 4px;
  border-radius: 10px;
  background: ${props => `linear-gradient( 135deg, transparent 30px, ${props.color} 0) top, left;`};
  background-repeat: no-repeat;
`;

const Border = props => {
  return (
    <StyledBorder
      color={props.color}
      onClick={props.onClick}
      onMouseEnter={props.onMouseEnter}
      onMouseLeave={props.onMouseLeave}
    >
      {props.children}
    </StyledBorder>
  );
};

export default Border;

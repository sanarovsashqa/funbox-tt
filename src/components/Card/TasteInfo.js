import styled from 'styled-components';

const TasteInfo = styled.h3`
  margin: 0;
  margin-bottom: 15px;
  font-size: 24px;
  color: var(--black-color);
  overflow: hidden;
  text-overflow: ellipsis;
`;

export default TasteInfo;

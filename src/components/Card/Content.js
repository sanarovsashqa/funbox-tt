import styled from 'styled-components';

const Content = styled.div`
  width: 312px;
  min-height: 472px;
  padding-top: 15px;
  padding-right: 15px;
  padding-bottom: 270px;
  padding-left: 45px;
  border-radius: 8px;
  background-color: var(--light-gray-color);
  background: url('img/card-bg.png') no-repeat bottom left,
    linear-gradient(135deg, transparent 29px, var(--light-gray-color) 0) top, left;
  opacity: ${props => (props.isDisabled ? 0.5 : 1)};
`;

export default Content;

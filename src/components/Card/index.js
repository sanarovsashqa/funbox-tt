import React from 'react';
import Wrapper from './Wrapper';
import Border from './Border';
import Content from './Content';
import Slogan from './Slogan';
import Name from './Name';
import TasteInfo from './TasteInfo';
import Portions from './Portions';
import Gifts from './Gifts';
import Client from './Client';
import Weight from './Weight';
import Description from './Description';
import { color } from 'constants';

class Card extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isSelected: false,
      isDisabled: props.data.inStock === 0,
      isMouseOver: false,
      isFirstHover: true
    };
  }

  getCardColor() {
    const { isSelected, isDisabled, isMouseOver, isFirstHover } = this.state;

    if (isDisabled) {
      return color.card.DISABLED;
    } else if (isSelected) {
      if (isMouseOver && !isFirstHover) {
        return color.cardHover.SELECTED;
      }

      return color.card.SELECTED;
    }

    return isMouseOver ? color.cardHover.DEFAULT : color.card.DEFAULT;
  }

  onCardClick = () => {
    const { isSelected, isDisabled } = this.state;
    if (!isDisabled) {
      this.setState({ isSelected: !isSelected });

      if (!isSelected) {
        this.setState({ isFirstHover: true });
      }
    }
  };

  onLinkClick = () => {
    const { isSelected, isDisabled } = this.state;
    if (!isDisabled) {
      this.setState({ isSelected: !isSelected });

      if (!isSelected) {
        this.setState({ isFirstHover: false });
      }
    }
  };

  onMouseEnter = () => {
    const { isDisabled } = this.state;
    if (!isDisabled) {
      this.setState({ isMouseOver: true });
    }
  };

  onMouseLeave = () => {
    const { isDisabled, isSelected } = this.state;
    if (!isDisabled) {
      this.setState({ isMouseOver: false });

      if (isSelected) {
        this.setState({ isFirstHover: false });
      }
    }
  };

  render() {
    const { data } = this.props;
    const { isSelected, isDisabled, isMouseOver, isFirstHover } = this.state;

    return (
      <Wrapper>
        <Border
          color={this.getCardColor()}
          onClick={this.onCardClick}
          onMouseEnter={this.onMouseEnter}
          onMouseLeave={this.onMouseLeave}
        >
          <Content isDisabled={isDisabled}>
            <Slogan
              isMouseOver={isMouseOver}
              isFirstHover={isFirstHover}
              isSelected={isSelected}
              slogan={data.slogan}
            />
            <Name>{data.name}</Name>
            <TasteInfo>{data.tasteInfo}</TasteInfo>
            <Portions portionsNum={data.portionsNum} />
            <Gifts giftsNum={data.giftsNum} />
            <Client>{data.clientSutisfied ? 'заказчик доволен' : null}</Client>
          </Content>
          <Weight color={this.getCardColor()} weight={data.weight} />
        </Border>
        <Description
          isSelected={isSelected}
          isDisabled={isDisabled}
          tasteInfo={data.tasteInfo}
          description={data.description}
          onClick={this.onLinkClick}
        />
      </Wrapper>
    );
  }
}

export default Card;

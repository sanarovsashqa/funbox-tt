import React from 'react';
import Main from 'components/Main';
import cardsData from 'data/cards.json';

const App = () => <Main data={cardsData} />;


export default App;
